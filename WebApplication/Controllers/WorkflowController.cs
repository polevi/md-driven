﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class WorkflowController : Controller
    {
        private Core.Configuration.ConfigurationService configurationService;

        public WorkflowController()
        {
            configurationService = Core.Configuration.ConfigurationService.Current;
        }

        public ActionResult DoAction(string name)
        {
            configurationService.DoAction(name);
            return Redirect(Request.UrlReferrer.ToString());
        }

    }
}