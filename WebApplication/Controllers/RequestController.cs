﻿using System.Web.Mvc;
using Core.Helpers;
using Core.Configuration;
using Core.Repositories;
using System.Linq;

namespace WebApplication.Controllers
{
    public class RequestController : Controller
    {
        private ConfigurationService configurationService;
        private RepositoryService repositoryService;

        public RequestController()
        {
            configurationService = ConfigurationService.Current;
            repositoryService = RepositoryService.Current;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Model = GetRepo().Create().ToDynamic();
            return View(configurationService.GetCurrentView().Base, configurationService);
        }

        [HttpGet]
        public ActionResult View(int id)
        {
            ViewBag.Model = GetRepo().CreateOrSelectById(id).ToDynamic();
            return View(configurationService.GetCurrentView().Base, configurationService);
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult Save(FormCollection values)
        {
            var d = values.AllKeys.ToDictionary(k => k, v => values[v]);
            int id = GetRepo().Update(int.Parse(d["Id"]), d.ToXmlDocument());
            return Redirect($"/Request/View/{id}");
        }

        private IRepository GetRepo()
        {
            return repositoryService.CreateRepository(configurationService.GetCurrentRepository().Type);
        }

    }
}