﻿CREATE PROCEDURE [dbo].[Request_Update] @Xml NTEXT, @Id INT OUT
AS
SET NOCOUNT ON
DECLARE @XmlId INT

EXEC sp_xml_preparedocument @XmlId OUT, @Xml

IF EXISTS(SELECT TOP 1 1 FROM dbo.[Request] WHERE [Id] = @Id)
	UPDATE dbo.Request SET
		[Category] = T.Category
	FROM dbo.Request R
	JOIN OPENXML(@XmlId, '/Root', 1) WITH (Id INT, Category NVARCHAR(100)) T ON R.Id = T.Id
ELSE
BEGIN
	INSERT INTO dbo.Request([Category])
	SELECT T.Category
	FROM OPENXML(@XmlId, '/Root', 1) WITH (Id INT, Category NVARCHAR(100)) T

	SET @Id = SCOPE_IDENTITY() 
END

EXEC sp_xml_removedocument @XmlId
GO