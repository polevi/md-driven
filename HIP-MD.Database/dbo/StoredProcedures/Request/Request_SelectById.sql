﻿CREATE PROCEDURE [dbo].[Request_SelectById] @Id INT
AS
SELECT
	1 AS Tag,
	NULL AS Parent,
	T.Id AS [Request!1!Id],
	T.Category AS  [Request!1!Category]
FROM dbo.Request T
WHERE Id = @Id
FOR XML EXPLICIT
GO
