﻿CREATE PROCEDURE [dbo].[RequestGeneral_SelectById] @Id INT
AS
SELECT
	1 AS Tag,
	NULL AS Parent,
	T.Id AS [Request!1!Id],
	T.Name AS  [Request!1!Name],
	T.Country AS  [Request!1!Country],
	T.[State] AS  [Request!1!State],
	T.[City] AS  [Request!1!City]
FROM dbo.RequestGeneral T
WHERE Id = @Id
FOR XML EXPLICIT
GO
