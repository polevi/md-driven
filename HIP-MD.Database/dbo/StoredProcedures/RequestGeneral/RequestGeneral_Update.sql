﻿CREATE PROCEDURE [dbo].[RequestGeneral_Update] @Id INT, @Xml NTEXT
AS
SET NOCOUNT ON
DECLARE @XmlId INT

EXEC sp_xml_preparedocument @XmlId OUT, @Xml

IF EXISTS(SELECT TOP 1 1 FROM dbo.[RequestGeneral] WHERE [Id] = @Id)
	UPDATE dbo.RequestGeneral SET
		[Name] = T.Name,
		[Country] = T.Country,
		[State] = T.[State],
		[City] = T.City
	FROM dbo.RequestGeneral R
	JOIN OPENXML(@XmlId, '/Root', 1) WITH (Id INT, Name NVARCHAR(100), Country NVARCHAR(100), [State] NVARCHAR(100), [City] NVARCHAR(100)) T ON R.Id = T.Id
ELSE
BEGIN
	INSERT INTO dbo.RequestGeneral([Id], [Name], [Country], [State], [City])
	SELECT @Id, T.Name, T.Country, T.[State], T.City
	FROM OPENXML(@XmlId, '/Root', 1) WITH (Id INT, Name NVARCHAR(100), Country NVARCHAR(100), [State] NVARCHAR(100), City NVARCHAR(100)) T

	SET @Id = SCOPE_IDENTITY() 
END

EXEC sp_xml_removedocument @XmlId
