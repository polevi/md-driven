﻿CREATE PROCEDURE [dbo].[RequestGeneral_Create] @Id INT
AS
SELECT
	1 AS Tag,
	NULL AS Parent,
	@Id AS [Request!1!Id],
	'' AS [Request!1!Name],
	'' AS [Request!1!Country],
	'' AS [Request!1!State],
	'' AS [Request!1!City]
FOR XML EXPLICIT
GO
