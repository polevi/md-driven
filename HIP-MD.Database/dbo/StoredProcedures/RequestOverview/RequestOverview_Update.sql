﻿CREATE PROCEDURE [dbo].[RequestOverview_Update] @Id INT, @Xml NTEXT
AS
SET NOCOUNT ON
DECLARE @XmlId INT

EXEC sp_xml_preparedocument @XmlId OUT, @Xml

IF EXISTS(SELECT TOP 1 1 FROM dbo.[RequestOverview] WHERE [Id] = @Id)
	UPDATE dbo.RequestOverview SET
		[Country] = T.Country,
		[BusinessUnit] = T.BusinessUnit,
		[Department] = T.Department,
		[Product] = T.Product
	FROM dbo.Request R
	JOIN OPENXML(@XmlId, '/Root', 1) WITH (Id INT, Country NVARCHAR(100), BusinessUnit NVARCHAR(100), Department NVARCHAR(100), Product NVARCHAR(100)) T ON R.Id = T.Id
ELSE
BEGIN
	INSERT INTO dbo.RequestOverview([Id],[Country],[BusinessUnit],[Department],[Product])
	SELECT @Id, T.Country, T.BusinessUnit, T.Department, T.Product
	FROM OPENXML(@XmlId, '/Root', 1) WITH (Id INT, Country NVARCHAR(100), BusinessUnit NVARCHAR(100), Department NVARCHAR(100), Product NVARCHAR(100)) T

	SET @Id = SCOPE_IDENTITY() 
END

EXEC sp_xml_removedocument @XmlId