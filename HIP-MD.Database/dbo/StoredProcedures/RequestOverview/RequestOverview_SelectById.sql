﻿CREATE PROCEDURE [dbo].[RequestOverview_SelectById] @Id INT
AS
SELECT
	1 AS Tag,
	NULL AS Parent,
	T.Id AS [Request!1!Id],
	T.Country AS  [Request!1!Country],
	T.BusinessUnit AS [Request!1!BusinessUnit],
	T.Department AS [Request!1!Department],
	T.Product AS [Request!1!Product]
FROM dbo.RequestOverview T
WHERE Id = @Id
FOR XML EXPLICIT
GO
