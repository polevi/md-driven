﻿CREATE PROCEDURE [dbo].[RequestOverview_Create] @Id INT
AS
SELECT
	1 AS Tag,
	NULL AS Parent,
	@Id AS [Request!1!Id],
	'' AS [Request!1!Name],
	'' AS [Request!1!Country],
	'' AS [Request!1!BusinessUnit],
	'' AS [Request!1!Department],
	'' AS [Request!1!Product]
FOR XML EXPLICIT
GO
