﻿CREATE TABLE [dbo].[RequestGeneral]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(100) NULL,
	[Country] NVARCHAR(100) NULL,
	[State] NVARCHAR(100) NULL,
	[City] NVARCHAR(100) NULL,
	CONSTRAINT FK_RequestGeneral_Request FOREIGN KEY([Id]) REFERENCES dbo.Request([Id])
)
