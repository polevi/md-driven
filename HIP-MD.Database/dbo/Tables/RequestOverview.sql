﻿CREATE TABLE [dbo].[RequestOverview]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Country] NVARCHAR(100) NULL,
	[BusinessUnit] NVARCHAR(100) NULL,
	[Department] NVARCHAR(100) NULL,
	[Product] NVARCHAR(100) NULL,
	CONSTRAINT FK_RequestOverview_Request FOREIGN KEY([Id]) REFERENCES dbo.Request([Id])
)
