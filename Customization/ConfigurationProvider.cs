﻿namespace Customization
{
    public static class ConfigurationProvider
    {
        public static System.IO.Stream GetConfigurationStream()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Customization.Configuration.xml");
        }
    }
}
