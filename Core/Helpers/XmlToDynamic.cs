﻿using System.Collections.Generic;
using System.Dynamic;
using System.Xml;

namespace Core.Helpers
{
    public static class XmlToDynamic
    {
        public static XmlDocument ToXmlDocument(this Dictionary<string,string> values, string root = "Root")
        {
            XmlDocument doc = new XmlDocument();

            XmlElement r = doc.CreateElement(string.Empty, root, string.Empty);

            foreach (var kvp in values)
            {
                var a = doc.CreateAttribute(kvp.Key);
                a.Value = kvp.Value;

                r.SetAttributeNode(a);
            }

            doc.AppendChild(r);

            return doc;
        }

        public static ExpandoObject ToDynamic(this XmlNode node)
        {
            return ParseXmlNode(new ExpandoObject(), node);
        }

        public static ExpandoObject ToDynamic(this XmlDocument doc)
        {
            return ParseXmlNode(new ExpandoObject(), doc.DocumentElement);
        }

        static ExpandoObject ParseXmlNode(dynamic obj, XmlNode node)
        {
            var p = obj as IDictionary<string, object>;

            foreach (XmlAttribute a in node.Attributes)
            {
                p[a.Name] = a.Value;
            }

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    List<ExpandoObject> lst = new List<ExpandoObject>();
                    ParseXmlNodeList(n.ChildNodes, lst);
                    p[n.Name] = lst;
                }
            }

            return obj;
        }

        static void ParseXmlNodeList(XmlNodeList nodes, List<ExpandoObject> container)
        {
            foreach (XmlNode n in nodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    var obj = new ExpandoObject();
                    container.Add(ParseXmlNode(obj, n));
                }
            }
        }
    }
}