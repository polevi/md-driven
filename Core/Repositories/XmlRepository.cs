﻿using System.Data.SqlClient;
using System.Xml;
using System;

namespace Core.Repositories
{
    public class XmlRepository : IRepository
    {
        private string connectionString;

        public XmlRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public virtual XmlDocument Create(int id = 0)
        {
            return GetDocument($"dbo.{ObjectName()}_Create", x => x.Parameters["@Id"].Value = id);
        }

        public virtual XmlDocument SelectById(int id)
        {
            return GetDocument($"dbo.{ObjectName()}_SelectById", x => x.Parameters["@Id"].Value = id);
        }

        public virtual XmlDocument CreateOrSelectById(int id)
        {
            var r = SelectById(id);
            return r.DocumentElement == null ? Create(id) : r;
        }

        public virtual int Update(int id, XmlDocument doc)
        {
            return PostDocument($"dbo.{ObjectName()}_Update", id, doc);
        }

        public virtual void Delete(int id)
        {
            throw new NotImplementedException();
        }

        private XmlDocument GetDocument(string procName, Action<SqlCommand> f = null)
        {
            var doc = new XmlDocument();

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var cmd = new SqlCommand(procName, conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (f != null)
                {
                    SqlCommandBuilder.DeriveParameters(cmd);
                    f(cmd);
                }

                using (var r = cmd.ExecuteXmlReader())
                {
                    doc.Load(r);
                }
            }

            return doc;
        }

        private int PostDocument(string procName, int id, XmlDocument doc)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var cmd = new SqlCommand(procName, conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                cmd.Parameters["@Id"].Value = id;
                cmd.Parameters["@xml"].Value = doc.OuterXml;
                cmd.ExecuteNonQuery();

                object rv = cmd.Parameters["@Id"].Value;
                return id != 0 ? id : (int)rv;
            }
        }

        private string ObjectName()
        {
            return GetType().Name.Replace("Repository", "");
        }
    }
}
