﻿using System.Dynamic;
using System.Xml;

namespace Core.Repositories
{
    public interface IRepository
    {
        XmlDocument Create(int id = 0);
        XmlDocument SelectById(int id);
        XmlDocument CreateOrSelectById(int id);
        int Update(int id, XmlDocument doc);
        void Delete(int id);
    }
}
