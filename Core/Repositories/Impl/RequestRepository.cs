﻿using System.Dynamic;
using System.Xml;

namespace Core.Repositories
{
    public class RequestRepository : XmlRepository
    {
        public RequestRepository(string connectionString) : base(connectionString)
        {
        }
    }
}
