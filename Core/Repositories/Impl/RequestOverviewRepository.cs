﻿using System.Dynamic;
using System.Xml;

namespace Core.Repositories
{
    public class RequestOverviewRepository : XmlRepository
    {
        public RequestOverviewRepository(string connectionString) : base(connectionString)
        {
        }
    }
}
