﻿using System.Dynamic;
using System.Xml;

namespace Core.Repositories
{
    public class RequestGeneralRepository : XmlRepository
    {
        public RequestGeneralRepository(string connectionString) : base(connectionString)
        {
        }
    }
}
