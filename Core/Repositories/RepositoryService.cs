﻿using System;
using System.Reflection;

namespace Core.Repositories
{
    public class RepositoryService
    {
        public static RepositoryService Current { get; private set; }
        private static string connectionString;

        public static RepositoryService CreateInstance(string connString)
        {
            if(Current == null)
            {
                connectionString = connString;
                Current = new RepositoryService();
            }

            return Current;
        }

        public IRepository CreateRepository(string typeName)
        {
            var t = Assembly.GetExecutingAssembly().GetType(typeName);
            var ci = t.GetConstructor(new Type[] { typeof(string) });

            return (IRepository)ci.Invoke(new object[] { connectionString });
        }
    }
}
