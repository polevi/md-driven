﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class Activity
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Assembly { get; set; }

        public Dictionary<string, string> Properties { get; }

        public Activity()
        {
            Properties = new Dictionary<string, string>();
        }
    }
}
