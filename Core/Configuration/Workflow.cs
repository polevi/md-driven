﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class Workflow
    {
        public string Name { get; set; }
        public string Condition { get; set; }

        public Dictionary<string, Step> Steps { get; }

        public Workflow()
        {
            Steps = new Dictionary<string, Step>();
        }
    }
}
