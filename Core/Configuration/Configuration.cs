﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class Configuration
    {
        public string Name { get; set; }
        public string Customer { get; set; }
        public string CoreVersion { get; set; }

        public Dictionary<string, Repository> Repositories { get; }
        public Dictionary<string, View> Views { get; }
        public Dictionary<string, Activity> Activities { get; }
        public Dictionary<string, BusinessProcess> BusinessProcesses { get; }

        public Configuration()
        {
            Repositories = new Dictionary<string, Repository>();
            Views = new Dictionary<string, View>();
            Activities = new Dictionary<string, Activity>();
            BusinessProcesses = new Dictionary<string, BusinessProcess>();
        }
    }
}
