﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class ViewItem
    {
        public string Name { get; set; }
        public bool Required { get; set; }
    }
}
