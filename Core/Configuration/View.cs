﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class View
    {
        public string Name { get; set; }
        public string Base { get; set; }

        public Dictionary<string, ViewItem> Items { get; }

        public View()
        {
            Items = new Dictionary<string, ViewItem>();
        }

    }
}
