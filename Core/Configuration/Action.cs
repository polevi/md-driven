﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class Action
    {
        public string Name { get; set; }
        public string Caption { get; set; }
        public string NextBusinessProcess { get; set; }
        public string NextStep { get; set; }

        public Dictionary<string, Activity> Activities { get; }

        public Action()
        {
            Activities = new Dictionary<string, Activity>();
        }
    }
}
