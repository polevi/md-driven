﻿using System.Xml;

namespace Core.Configuration
{
    public static class Loader
    {
        public static Configuration Load(System.IO.Stream s)
        {
            var doc = new XmlDocument();
            doc.Load(s);

            return Load(doc);
        }

        private static Configuration Load(XmlDocument doc)
        {
            var e = new Configuration();

            var node = doc.DocumentElement.SelectSingleNode("//Configuration");
            LoadProperties(e, node);
            LoadViews(e, node);
            LoadRepositories(e, node);
            LoadActivities(e, node);
            LoadBusinessProcesses(e, node);

            return e;
        }

        private static void LoadViews(Configuration e, XmlNode node)
        {
            var nodes = node.SelectNodes("Views/View");
            foreach(XmlNode n in nodes)
            {
                View view = new View();
                LoadProperties(view, n);
                LoadViewItems(view, n);
                e.Views.Add(view.Name, view);
            }
        }

        private static void LoadViewItems(View e, XmlNode node)
        {
            var nodes = node.SelectNodes("Item");
            foreach (XmlNode n in nodes)
            {
                ViewItem viewItem = new ViewItem();
                LoadProperties(viewItem, n);
                e.Items.Add(viewItem.Name, viewItem);
            }
        }

        private static void LoadRepositories(Configuration e, XmlNode node)
        {
            var nodes = node.SelectNodes("Repositories/Repository");
            foreach (XmlNode n in nodes)
            {
                Repository rep = new Repository();
                LoadProperties(rep, n);
                e.Repositories.Add(rep.Name, rep);
            }
        }

        private static void LoadActivities(Configuration e, XmlNode node)
        {
            var nodes = node.SelectNodes("Activities/Activity");
            foreach (XmlNode n in nodes)
            {
                Activity activity = new Activity();
                LoadProperties(activity, n);
                LoadActivityProperties(activity, n);
                e.Activities.Add(activity.Name, activity);
            }
        }

        private static void LoadActivityProperties(Activity e, XmlNode node)
        {
            var nodes = node.SelectNodes("Properties/Property");
            foreach (XmlNode n in nodes)
            {
                e.Properties.Add(n.Attributes["Name"].Value, n.Attributes["Value"].Value);
            }
        }

        private static void LoadBusinessProcesses(Configuration e, XmlNode node)
        {
            var nodes = node.SelectNodes("BusinessProcesses/BusinessProcess");
            foreach (XmlNode n in nodes)
            {
                BusinessProcess bp = new BusinessProcess();
                LoadProperties(bp, n);
                LoadWorkflows(bp, n);
                e.BusinessProcesses.Add(bp.Name, bp);
            }
        }

        private static void LoadWorkflows(BusinessProcess e, XmlNode node)
        {
            var nodes = node.SelectNodes("Workflow");
            foreach (XmlNode n in nodes)
            {
                Workflow wf = new Workflow();
                LoadProperties(wf, n);
                LoadWorkflowsteps(wf, n);
                e.Workflows.Add(wf.Name, wf);
            }
        }

        private static void LoadWorkflowsteps(Workflow e, XmlNode node)
        {
            var nodes = node.SelectNodes("Step");
            foreach (XmlNode n in nodes)
            {
                Step s = new Step();
                LoadProperties(s, n);
                LoadStepActions(s, n);
                e.Steps.Add(s.Name, s);
            }
        }

        private static void LoadStepActions(Step e, XmlNode node)
        {
            var nodes = node.SelectNodes("Action");
            foreach (XmlNode n in nodes)
            {
                Action a = new Action();
                LoadProperties(a, n);
                LoadStepActivities(a, n);
                e.Actions.Add(a.Name, a);
            }
        }

        private static void LoadStepActivities(Action e, XmlNode node)
        {
            var nodes = node.SelectNodes("Activities/Activity");
            foreach (XmlNode n in nodes)
            {
                Activity a = new Activity();
                LoadProperties(a, n);
                e.Activities.Add(a.Name, a);
            }
        }

        private static void LoadProperties(object e, XmlNode node)
        {
            var type = e.GetType();

            foreach (XmlAttribute a in node.Attributes)
            {
                var prop = type.GetProperty(a.Name, System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
                if (prop != null)
                {
                    object v = System.Convert.ChangeType(a.Value, prop.PropertyType);
                    prop.SetValue(e, v);
                }
            }
        }
    }
}
