﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Core.Configuration
{
    public class ConfigurationService
    {
        public static ConfigurationService Current { get; private set; }
        public Configuration Configuration { get; private set; }
        public string[] CurrentStep { get; private set; }

        public static ConfigurationService CreateInstance(System.IO.Stream s)
        {
            if (Current == null)
            {
                Current = new ConfigurationService();
                Current.Configuration = Loader.Load(s);
            }

            //move to begin
            Current.GoToTheFirstStep();

            return Current;
        }

        private void GoToTheFirstStep(BusinessProcess bProcess = null)
        {
            var bp = bProcess ?? Current.Configuration.BusinessProcesses.First().Value;
            var wf = bp.Workflows.First().Value;
            var step = wf.Steps.First().Value;

            CurrentStep = new string[] { bp.Name, wf.Name, step.Name };
        }

        public BusinessProcess GetCurrentBusinessProcess()
        {
            return Current.Configuration.BusinessProcesses[CurrentStep[0]];
        }

        public Workflow GetCurrentWorkflow()
        {
            return GetCurrentBusinessProcess().Workflows[CurrentStep[1]];
        }

        public Step GetCurrentStep()
        {
            return GetCurrentWorkflow().Steps[CurrentStep[2]];
        }

        public View GetCurrentView()
        {
            string view = GetCurrentStep().View;
            return Current.Configuration.Views[view];
        }

        public Repository GetCurrentRepository()
        {
            string repo = GetCurrentStep().Repository;
            return Current.Configuration.Repositories[repo];
        }

        public void DoAction(string name)
        {
            var step = GetCurrentStep();
            var action = step.Actions[name];

            if (!string.IsNullOrEmpty(action.NextStep))
            {
                CurrentStep[2] = GetCurrentWorkflow().Steps[action.NextStep].Name;
                return;
            }

            if (!string.IsNullOrEmpty(action.NextBusinessProcess))
            {
                GoToTheFirstStep(Current.Configuration.BusinessProcesses[action.NextBusinessProcess]);
                return;
            }
        }
    }
}
