﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class Step
    {
        public string Name { get; set; }
        public string ResponsibleGroup { get; set; }
        public string View { get; set; }
        public string Repository { get; set; }

        public Dictionary<string, Action> Actions { get; }

        public Step()
        {
            Actions = new Dictionary<string, Action>();
        }
    }
}
