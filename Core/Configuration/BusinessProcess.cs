﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class BusinessProcess
    {
        public string Name { get; set; }

        public Dictionary<string, Workflow> Workflows { get; set; }

        public BusinessProcess()
        {
            Workflows = new Dictionary<string, Workflow>();
        }
    }
}
