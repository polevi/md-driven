﻿namespace Core.Configuration
{
    public class Repository
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
